#!/bin/bash
#Esse script recebe o nome de um arquivo encriptografado como linha de comando e descriptografa

rm -rf /tmp/temporario
tac $1 | tr 'cdefghijklmnopqrstuvwxyzabCDEFGHIJKLMNOPQRSTUVWXYZAB' 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ' > /tmp/temporario
cat /tmp/temporario > $1
