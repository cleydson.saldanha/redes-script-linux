#!/bin/bash
#Esse script recebe como parâmetro de linha de comando o nome do arquivo onde vai salvar os linhas geradas a partir do comando "sort -R blablabla"
rm -rf blabla
sort -R blablabla | head -$1 > blabla
echo 'O arquivo "blabla" foi criado, contendo a quantidade de linhas selecionadas'
sleep 2
