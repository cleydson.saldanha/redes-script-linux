#!/bin/bash

rm -rf /tmp/temporario
tac $1 | tr 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ' 'cdefghijklmnopqrstuvwxyzabCDEFGHIJKLMNOPQRSTUVWXYZAB' > /tmp/temporario
cat /tmp/temporario > $1
