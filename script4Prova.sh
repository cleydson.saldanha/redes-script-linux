#!/bin/bash

menu(){
	echo 'a) Selecionar arquivo'
	echo 'b) Preview do arquivo'
	echo 'c) Exibir o arquivo selecionado'
	echo 'd) Criar um arquivo com N linhas de blablabla'
	echo 'e) Pesquisa no arquivo selecionado'
	echo 'f) Criar uma cópia do arquivo selecionado'
	echo 'g) Sair'
	sleep 1
}

while [ ! "$opcao" = 'g' ]; do
	menu
	read -p 'Selecione uma opção do menu: ' opcao
	case $opcao in
		a) read -p 'Selecione um arquivo: ' arq ;;
		b) if test -n "$arq"; then
		       	head -2 $arq 
		       	tail -2 $arq
			fi	;;
		c) if test -n "$arq"; then
		       	cat $arq
			fi	;;
		d) if test -n "$arq"; then
			read -p 'Informe a quantidade de linhas do arquivo: ' linhas; ./script1Prova.sh $linhas
			fi	;;
		e) if test -n "$arq"; then
		        read -p 'Digite o que deseja buscar no arquivo: ' busca;cat $arq | grep "$busca"
			fi	;;
		f) if test -n "$arq"; then
		        cp $arq $arq.bkp
			fi	;;
	esac
done
